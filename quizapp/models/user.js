var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    role: { type: Number, default: 2 },
    first_name: { type: String, required: true, max: 100 },
    family_name: { type: String, required: true, max: 100 },
    username: { type: String, required: true, max: 100, index: { unique: true } },
    password: { type: String, required: true, max: 100 },
    score: { type: Number, default: 0 }
});

// Virtual for user's full name
UserSchema
    .virtual('name')
    .get(function () {
        return this.family_name + ', ' + this.first_name;
    });

//?????
// Virtual for user's URL
UserSchema
    .virtual('url')
    .get(function () {
        return '/catalog_???/user/' + this._id;
    });

//Export model
module.exports = mongoose.model('User', UserSchema);