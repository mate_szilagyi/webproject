var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var QuestionSchema = new Schema({
    question: { type: String, required: true },
    correct: { type: String, required: true },
    wrong: { type: Array, required: true }
});

// Virtual for question's URL
QuestionSchema
    .virtual('url')
    .get(function () {
        return '/question/' + this._id;
    });

//Export model
module.exports = mongoose.model('Question', QuestionSchema);