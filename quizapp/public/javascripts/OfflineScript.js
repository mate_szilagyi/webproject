function checkServerStatus() {
	$.ajax({
		url: '/alive',
		success: function () {
			(window.location.pathname == "/login") ? SignInFromLocalStorage() : RegisterFromLocalStorage();
		},
		error: function () {
			setTimeout(checkServerStatus, 1000);
		}, timeout: 1000
	});
}
function SignInFromLocalStorage() {
	if (localStorage.getItem("username") && localStorage.getItem("pwd")) {
		$('#userName').val(localStorage.getItem("username"));
		$('#pwd').val(localStorage.getItem("pwd"));
		localStorage.clear();
		$('#messages').html('');
		$("#login-form").submit();
	}
}

function RegisterFromLocalStorage() {
	if (localStorage.getItem("username") != null &&
		localStorage.getItem("pwd") != null &&
		localStorage.getItem("firstname") != null &&
		localStorage.getItem("familyname") != null &&
		localStorage.getItem("confpwd") != null) {
		$('#userName').val(localStorage.getItem("username"));
		$('#pwd').val(localStorage.getItem("pwd"));
		$('#firstName').val(localStorage.getItem("firstname"));
		$('#familyName').val(localStorage.getItem("familyname"));
		$('#confPwd').val(localStorage.getItem("confPwd"));
		localStorage.clear();
		$('#messages').html('');
		$("#register-form").submit();
	}
}

$(function () {
	$('#register-btn').click(function (event) {
		event.preventDefault();
		$.ajax({
			url: '/alive',
			success: function () {
				localStorage.clear();
				$('#messages').html('');
				$("#register-form").submit();
			}, error: function () {
				appendOfflineWarningMessage();
				localStorage.setItem("username", $('#userName').val());
				localStorage.setItem("pwd", $('#pwd').val());
				localStorage.setItem("firstname", $('#firstName').val());
				localStorage.setItem("familyname", $('#familyName').val());
				localStorage.setItem("confpwd", $('#confPwd').val());
				checkServerStatus();
			},
			timeout: 1000
		})
	})

	$('#sign-in-btn').click(function (event) {
		event.preventDefault();
		console.log("wtf");
		$.ajax({
			url: '/alive',
			success: function () {
				console.log('wtf1');
				localStorage.clear();
				console.log('wtf2');
				$('#messages').html('');
				$("#login-form").submit();
			}, error: function () {
				console.log('wtf3');
				appendOfflineWarningMessage();

				localStorage.setItem("username", $('#userName').val());
				localStorage.setItem("pwd", $('#pwd').val());
				checkServerStatus();
			},
			timeout: 1000
		})
	})
})

function appendOfflineWarningMessage() {
	var newdiv = $('<div/>', {
		id: 'warning-message',
		class: 'alert alert-warning fade in alert-dismissible',
		style: 'margin-top:18px',
		text: "Oops, Looks like our server is currently down. Your input has been saved and will be processed as soon as we get back!",
	});
	var newa = $('<a/>', {
		class: 'close',
		href: '#',
	});
	newa.attr('data-dismiss', 'alert');
	newa.attr('aria-label', 'close');
	newa.attr('title', 'close');
	newa.html('×');
	newdiv.prepend(newa);
	$('#messages').prepend(newdiv);
}