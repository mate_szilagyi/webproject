function editQuestionCompleteFields(questionId) {
	clearMessageDivs();
	$('#modalForm').attr('action', '/control/editQuestion');
	$('#modal_title').html('Edit Question');
	$("#modal_question").val($('#question' + questionId).html());
	$("#modal_correct").val($('#correct' + questionId).html());
	$("#modal_wrong1").val($('#wrong1' + questionId).html());
	$("#modal_wrong2").val($('#wrong2' + questionId).html());
	$("#modal_wrong3").val($('#wrong3' + questionId).html());
	$("#questionId").val(questionId);
}

function copyQuestionIdToModal(questionId) {
	$('#delete-modal-btn').prop('disabled', false);
	$("#delete-questionId").val(questionId);
	if ($('#success-message').length) {
		$('#success-message').remove();
	}
}
function clearMessageDivs() {
	if ($('#success-message').length) {
		$('#success-message').remove();
	}
	if ($('#error-message').length) {
		$('#error-message').remove();
	}
}

//type 'danger' || 'success'
function appendDismissibleMessage(divId, message, type) {
	clearMessageDivs();
	var newdiv = $('<div/>', {
		id: type + '-message',
		class: 'alert alert-' + type + ' fade in alert-dismissible text-left',
		style: 'margin-top:18px',
		text: message,
	});
	var newa = $('<a/>', {
		class: 'close',
		href: '#',
	});
	newa.attr('data-dismiss', 'alert');
	newa.attr('aria-label', 'close');
	newa.attr('title', 'close');
	newa.html('×');
	newdiv.append(newa);
	$(divId).prepend(newdiv);
}

$(function () {
	$('#addNewQuestionBtn').click(function () {
		clearMessageDivs();
		$('form').attr("action", "/control/addNewQuestion");
		$('#modal_title').html('Add new Question');
		$('#modal_question').val('');
		$('#modal_correct').val('');
		$('#modal_wrong1').val('');
		$('#modal_wrong2').val('');
		$('#modal_wrong3').val('');
		$('#questionId').val('');
		$('#modal_question').val('');
	});

	$('#get-questions-button').on('click', function () {
		$.ajax({
			url: '/control/listQuestions',
			contentType: 'application/json',
			success: function (response) {
				var tableBody = $('#table-body');
				tableBody.html('');
				$('#table-header').html('');
				response.questions.forEach(function (question) {
					tableBody.append('\
					<tr>\
						<div class="card">\
						<td style="width:70%;">\
							<div class="card-header" id="heading_'+ question._id + '">\
								<h5 class="mb-1">\
									<button style=" white-space: inherit;text-align:left;" class="btn btn-link collapsed" id="question_'+ question._id + '" data-target="#collapse_' + question._id + '" data-toggle="collapse" aria-controls="collapse_' + question._id + '" aria-expanded="false">' + question.question + '</button>\
								</h5>\
							</div>\
							<div class="collapse" id="collapse_'+ question._id + '" data-parent="#table-body" aria-labelledby="heading_' + question._id + '">\
								<div class="card-body">\
									<p class="text-success" id="correct_'+ question._id + '">' + question.correct + '</p>\
									<p class="text-danger" id="wrong1_'+ question._id + '">' + question.wrong[0] + '</p>\
									<p class="text-danger" id="wrong2_'+ question._id + '">' + question.wrong[1] + '</p>\
									<p class="text-danger" id="wrong3_'+ question._id + '">' + question.wrong[2] + '</p>\
								</div>\
							</div>\
							<td class="column-verticallineMiddle form-inline" style="width:30%;" align="right">\
							<button class="btn btn-warning a-btn-slide-text" id="edit_'+ question._id + '" data-target="#myModal" data-toggle="modal" onclick="editQuestionCompleteFields(&quot;_' + question._id + '&quot;)" >\
										<span class="glyphicon glyphicon-edit" aria-hidden="true">\
											<p style="font-family:Helvetica; display:inline;"> Edit </p>\
										</span>\
									</button>\
									<button class="btn btn-danger a-btn-slide-text" data-toggle="modal" data-target="#deleteConfirmationModal" id="trash_'+ question._id + '" onclick="copyQuestionIdToModal(&quot;_' + question._id + '&quot;)" >\
										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\
									</button></td>\
						</div>\
					</tr>\
				');
				});
			}
		});
	});

	$('#set-quiz-length-btn').on('click', function (event) {
		var len = $('#default-quiz-length-txt').val();
		if (Math.floor(len) != len && !($.isNumeric(len))) {
			appendDismissibleMessage("#messages", "The value you entered is not valid, please enter a number!", "danger")
		} else {
			if (len < 1 || len > 100) {
				appendDismissibleMessage("#messages", "The value you entered is not valid, please enter a maximum two digit positive number!", "danger")
			} else {
				$.ajax({
					url: '/control/setQuizLen',
					contentType: 'application/json',
					method: 'POST',
					data: JSON.stringify({ len: len }),
					success: function (response) {
						clearMessageDivs();
						if (response.error) {
							appendDismissibleMessage('#messages', response.error, "danger");
						}
						if (response.message) {
							appendDismissibleMessage('#messages', response.message, "success");
						}
					}
				});
			}
		}
	});

	$('#get-users-button').on('click', function () {
		$.ajax({
			url: '/control/listUsers',
			contentType: 'application/json',
			success: function (response) {
				var tableBody = $('#table-body');
				tableBody.html('');
				var tableHeader = $('#table-header');
				tableHeader.html('');
				tableHeader.append('\
					<tr>\
						<th>First Name</th>\
						<th>Family Name</th>\
						<th scope="col">Username</th>\
						<th scope="col">Score<th>\
					</tr>');
				response.users.forEach(function (user) {
					tableBody.append('\
					<tr>\
					<td>'+ user.first_name + '</td>\
					<td>'+ user.family_name + '</td>\
					<td>'+ user.username + '</td>\
					<td>'+ user.score + '</td>\
					</tr>\
					');
				})
			}
		})
	})

	$('#delete-modal-btn').on('click', function () {
		var questionId = $('#delete-questionId').val();
		$.ajax({
			url: '/control/deleteQuestion',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify({ questionId: questionId }),
			success: function (response) {
				appendDismissibleMessage("#delete-modal-footer", response.message, "success")
				$('#delete-modal-btn').prop('disabled', true);
				$('#get-questions-button').click();
			}
		})
	});

	$('#submit-modal-btn').on('click', function (event) {
		event.preventDefault();
		var questionId = $('#questionId').val();
		var question = $('#modal_question').val();
		var correct = $('#modal_correct').val();
		var wrong1 = $('#modal_wrong1').val();
		var wrong2 = $('#modal_wrong2').val();
		var wrong3 = $('#modal_wrong3').val();
		var action = $('#modalForm').attr('action');
		$.ajax({
			url: action,
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify({ questionId: questionId, question: question, correct: correct, wrong1: wrong1, wrong2: wrong2, wrong3: wrong3 }),
			success: function (response) {
				if (response.message) {
					clearMessageDivs();
					appendDismissibleMessage("#modal-body", response.message, "success");
					if ($('#table-body').htnl().length > 0) {
						$('#get-questions-button').click();
					}
				}
				if (response.errors) {
					for (var i in response.errors) {
						appendDismissibleMessage("#modal-body", response.errors[i].msg, "danger");
					}
				}
			}
		})
	});
});