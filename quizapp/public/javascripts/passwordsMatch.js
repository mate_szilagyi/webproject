$(function () {
	$('#pwd').on('keyup', function () {
		if ($('#pwd').val() != $('#confPwd').val()) {
			$('#passwordsmatch').attr('class', 'alert alert-danger');
			$('#passwordsmatch').html('<p>Passwords don\'t match</p>');
		} else {
			$('#passwordsmatch').attr('class', 'alert alert-info');
			$('#passwordsmatch').html('<p>Passwords match</p>');
		}
		if ($('#confPwd').val() == "" || $('#pwd').val() == "") {
			$('#passwordsmatch').attr('class', '');
			$('#passwordsmatch').html('');
		}
	}); $('#confPwd').on('keyup', function () {
		if ($('#pwd').val() != $('#confPwd').val()) {
			$('#passwordsmatch').attr('class', 'alert alert-danger');
			$('#passwordsmatch').html('<p>Passwords don\'t match</p>');
		} else {
			$('#passwordsmatch').attr('class', 'alert alert-info');
			$('#passwordsmatch').html('<p>Passwords match</p>');
		}
		if ($('#confPwd').val() == "" || $('#pwd').val() == "") {
			$('#passwordsmatch').attr('class', '');
			$('#passwordsmatch').html('');
		}
	});
})