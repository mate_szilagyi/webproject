function shuffleArray(array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
	return array;
}
function msToTime(s) {
	var ms = s % 1000;
	s = (s - ms) / 1000;
	var secs = s % 60;
	s = (s - secs) / 60;
	var mins = s % 60;
	var hrs = (s - mins) / 60;
	if (hrs != 0) return hrs + ' hours ' + mins + ' mins ' + secs + '.' + ms + ' seconds';
	if (mins != 0) return mins + ' mins ' + secs + '.' + ms + ' seconds';
	return secs + '.' + ms + ' seconds';
}

$(function () {
	$.ajax({
		url: '/quiz/getQuizQuestions',
		contentType: 'application/json',
		success: function (response) {
			var questions = response.questions;
			$.ajax({
				url: '/quiz/getQuizLength',
				contentType: 'application/json',
				success: function (response) {
					var max = response.len;
					var randNr = Math.floor((Math.random() * questions.length));
					var counter = 1;
					var randomQuestions = [];
					randomQuestions.push(questions[randNr]);
					while (counter < max) {
						randNr = Math.floor((Math.random() * questions.length));
						if (randomQuestions.indexOf(questions[randNr]) == -1) {
							randomQuestions.push(questions[randNr]);
							counter++;
						}
					}
					var answers = [];
					var index = 0;
					var correct = 0;
					answers.push(randomQuestions[index].correct);
					for (j = 0; j < 3; j++) {
						answers.push(randomQuestions[index].wrong[j]);
					}
					answers = shuffleArray(answers);
					$('#progress').html('1 of ' + max);
					$('#a').val(answers[0]);
					$('#b').val(answers[1]);
					$('#c').val(answers[2]);
					$('#d').val(answers[3]);
					$('#question').html(randomQuestions[index].question);
					$('#container').show();
					var stratGame = new Date().getTime();
					$('input[type="button"]').click(function (e) {
						if (e.target.value == randomQuestions[index].correct) {
							correct++;
						}
						index++;
						if (index < randomQuestions.length) {
							answers = [];
							answers.push(randomQuestions[index].correct);
							for (j = 0; j < 3; j++) {
								answers.push(randomQuestions[index].wrong[j]);
							}
							answers = shuffleArray(answers);
							$('#progress').html((index + 1) + ' of ' + max);
							$('#a').val(answers[0]);
							$('#b').val(answers[1]);
							$('#c').val(answers[2]);
							$('#d').val(answers[3]);
							$('#question').html(randomQuestions[index].question);
						} else {
							$.post('/quiz/userSaveScore', { score: correct });
							$('#container').html('');
							var newDiv = $('<div/>', {
								id: 'success-message',
								class: 'col-md-offset-3 col-md-8 alert alert-success fade in alert-dismissible',
								style: 'margin-top:18px',
								html: "<p>Congratulations! You answered correctly " + correct + " out of " + index + " questions</p><p>It took you " + msToTime(new Date().getTime() - stratGame) + " to finish the quizz</p>",
							});
							$('#container').prepend(newDiv);
							var newA = $('<a/>', {
								href: '/quiz',
								class: 'btn btn-success btn-lg col-md-offset-3 col-md-8',
								text: 'Again!'
							});
							$('#container').append(newA);
						}
					});
				}
			});
		}
	});
})();