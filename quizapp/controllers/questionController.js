var Question = require('../models/question.js');
var db = require('../db.js');

exports.question_create_post = function (req, res) {
	var question = new Question({
		question: req.body.question,
		correct: req.body.correct,
		wrong: [req.body.wrong1, req.body.wrong2, req.body.wrong3]
	});
	db.collection("questions").insertOne(question, function (err, result) {
		if (err) throw err;
		res.send({ message: "Item Successfully inserted!" });
	});
}

exports.question_get_all = function (req,res) {
	Question.find({}, function (err, result) {
		if (err) throw err;
		res.send({ questions: result });
	});
}

exports.question_edit_by_id_post = function (req, res) {
	Question.findByIdAndUpdate(req.body.questionId.substring(1), {
		$set: {
			question: req.body.question,
			correct: req.body.correct,
			wrong: [req.body.wrong1, req.body.wrong2, req.body.wrong3]
		}
	}, function (err, result) {
		if (err) throw err;
		res.send({ message: "Item Successfully Edited!" });
	});
}

exports.question_delete_by_id_post = function (req, res) {
	Question.deleteOne({ _id: req.body.questionId.substring(1) }, function (err) {
		if (err) throw err;
		res.send({ message: "Item Successfully Deleted!" });
	});
}

exports.question_count = function (callback) {
	Question.count(function (err, c) {
		if (err) return err;
		callback(null, c)
	});
}