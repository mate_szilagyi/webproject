var User = require('../models/user.js');
var db = require('../db.js');
const bcrypt = require('bcrypt');

exports.user_find_by_username_and_password = function (req, res) {
	db.collection("users").findOne({ username: req.body.userName }, function (err, result) {
		if (err) throw err;
		if (result) {
			var user = {
				firstName: result.first_name,
				familyName: result.family_name,
				username: result.username,
				role: result.role
			};
			bcrypt.compare(req.body.pwd, result.password, function (err, result) {
				if (result) {
					session = req.session;
					session.uniqueID = user;
					if (user.role === 1) {
						res.redirect('/control');
					} else {
						res.redirect('/home');
					}
				} else {
					// Passwords don't match
					var errors = [];
					errors.push({ msg: "Username and Password does not match" });
					res.render('login', {
						errorMsg: errors,
					});
				}
			});
		} else {
			// User not found in database
			var errors = [];
			errors.push({ msg: "Username and Password does not match" });
			res.render('login', {
				errorMsg: errors,
			});
		}
	});
}

exports.user_create_post = function (req, res) {
	db.collection("users").findOne({ username: req.body.userName }, function (err, result) {
		if (err) throw err;
		if (result) {
			var errors = [];
			errors.push({ msg: "Username already taken!" });
			res.render('register', {
				firstName: req.body.firstName,
				familyName: req.body.familyName,
				userName: req.body.userName,
				errorMsg: errors,
				message: ""
			});
		} else {
			bcrypt.hash(req.body.pwd, 7, function (err, hash) {
				if (err) throw err;
				if (hash) {
					user = new User({
						first_name: req.body.firstName,
						family_name: req.body.familyName,
						username: req.body.userName,
						password: hash
					});
					db.collection("users").insertOne(user, function (err, result) {
						if (err) throw err;
						res.render('register', {
							firstName: req.body.firstName,
							familyName: req.body.familyName,
							userName: req.body.userName,
							errorMsg: "",
							message: "Account Successfully created! You can login now."
						});
					});
				}
			});
		}
	});
}

exports.user_get_all = function (req, res) {
	User.find({}, function (err, result) {
		if (err) throw err;
		res.send({ users: result });
	});
}
exports.user_save_score = function (req, res) {
	User.findOneAndUpdate({ username: req.session.uniqueID.username }, { $inc: { score: req.body.score } }).exec(function (err, result) {
		if (err) throw err;
	});
}