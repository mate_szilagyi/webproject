var express = require('express');
var router = express.Router();
var questionController = require('../controllers/questionController');
var userController = require('../controllers/userController');
var config = require('../config.json');

router.get('/', function (req, res, next) {
    res.render('quiz', { user: session.uniqueID });
});
router.get('/getQuizQuestions', function (req, res) {
    questionController.question_get_all(req, res);
});
router.get('/getQuizLength', function (req, res) {
    res.send({ len: config.default_quiz_length });
});

router.post('/userSaveScore', function (req, res) {
    userController.user_save_score(req, res);
});

module.exports = router;
