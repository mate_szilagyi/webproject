var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');
const { check, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

router.get('/', function (req, res, next) {
	res.render('register', { firstName: "", familyName: "", userName: "", errorMsg: "", message: "" });
});

router.post('/', [
	check('firstName', 'Please complete first name field').isLength({ min: 1 }),
	check('firstName', 'First name too long').isLength({ max: 50 }),
	check('firstName', 'First name can contain only letters').isAlpha(),
	check('familyName', 'Please complete family name field').isLength({ min: 1 }),
	check('familyName', 'Family name too long').isLength({ max: 50 }),
	check('familyName', 'Family name can contain only letters').isAlpha(),
	check('userName', 'Username min:6 max:20 characters').isLength({ min: 6, max: 20 }),
	check('userName', 'Username must start with letter. Allowed characters for username: a-z A-Z 0-9 - _ .').matches('^[a-zA-Z][a-zA-Z0-9-_\.]*$'),
	check('pwd', 'Password min:6 max:20 characters').isLength({ min: 6, max: 20 }),
	check('confPwd', 'Password confirmation does not match password').custom((value, { req }) => value === req.body.pwd),
	sanitizeBody('firstName').trim().escape(),
	sanitizeBody('familyName').trim().escape(),
	sanitizeBody('userName').trim().escape(),
	sanitizeBody('pwd').trim().escape()
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.render('register', {
			firstName: req.body.firstName,
			familyName: req.body.familyName,
			userName: req.body.userName,
			errorMsg: errors.array(),
			message: ""
		});
	} else {
		console.log("create user");
		userController.user_create_post(req, res);
	}
});

module.exports = router;
