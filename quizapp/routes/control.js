var express = require('express');
var router = express.Router();
var questionController = require('../controllers/questionController');
var userController = require('../controllers/userController');
var fs = require('fs');
var config = require('../config.json');
const { sanitizeBody } = require('express-validator/filter');
const { check, validationResult } = require('express-validator/check');

/* GET users listing. */
router.get('/', function (req, res) {
	res.render('control', { user: session.uniqueID, quizLength: config.default_quiz_length });
});

router.post('/addNewQuestion', [
	check('question', 'You must complete Question field!').isLength({ min: 1, max: 100 }),
	check('correct', 'You must complete Correct answer field!').isLength({ min: 1, max: 100 }),
	check('wrong1', 'You must complete first Wrong answer field!').isLength({ min: 1, max: 100 }),
	check('wrong2', 'You must complete second Wrong answer field!').isLength({ min: 1, max: 100 }),
	check('wrong3', 'You must complete third Wrong answer field!').isLength({ min: 1, max: 100 }),
	sanitizeBody('question').trim().escape(),
	sanitizeBody('wrong1').trim().escape(),
	sanitizeBody('wrong2').trim().escape(),
	sanitizeBody('wrong3').trim().escape()
], (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.send({ errors: errors.array().reverse() });
	} else {
		questionController.question_create_post(req, res);
	}
});

router.post('/editQuestion', [
	check('question', 'You must complete Question field!').isLength({ min: 1, max: 100 }),
	check('correct', 'You must complete Correct answer field!').isLength({ min: 1, max: 100 }),
	check('wrong1', 'You must complete first Wrong answer fields!').isLength({ min: 1, max: 100 }),
	check('wrong2', 'You must complete second Wrong answer fields!').isLength({ min: 1, max: 100 }),
	check('wrong3', 'You must complete third Wrong answer fields!').isLength({ min: 1, max: 100 }),
	sanitizeBody('questionId').trim().escape(),
	sanitizeBody('question').trim().escape(),
	sanitizeBody('wrong1').trim().escape(),
	sanitizeBody('wrong2').trim().escape(),
	sanitizeBody('wrong3').trim().escape()
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.send({ errors: errors.array().reverse() });
	} else {
		questionController.question_edit_by_id_post(req, res);
	}
});

router.get('/listQuestions', function (req, res) {
	questionController.question_get_all(req, res);
});

router.post('/deleteQuestion', [
	sanitizeBody('questionId').trim().escape()
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.send({ errors: errors.array() });
	} else {
		questionController.question_delete_by_id_post(req, res);
	}
});

router.get('/listUsers', function (req, res) {
	userController.user_get_all(req, res);
});

router.post('/setQuizLen', [
	sanitizeBody('len').trim().escape()
], (req, res) => {
	questionController.question_count(function (err, result) {
		if (parseInt(req.body.len) > result) {
			res.send({ error: "Sorry! there is not enough questions in the database!" });
		} else {
			config.default_quiz_length = parseInt(req.body.len);
			fs.writeFile('config.json', JSON.stringify(config, null, 2), function (err) {
				if (err) {
					res.send({ error: "Something went wrong, operation could not be completed!" });
					return console.log(err);
				}
				res.send({ message: "Number of questions in a quiz successfully set!" });
			});
		}
	})
});

module.exports = router;
