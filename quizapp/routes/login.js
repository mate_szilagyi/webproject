var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');
const { sanitizeBody } = require('express-validator/filter');
const { check, validationResult } = require('express-validator/check');

/* GET users listing. */
router.get('/', function (req, res, next) {
	res.render('login');
});

router.post('/', [
	check('userName', 'Please complete username field').isLength({ min: 1 }),
	check('pwd', 'Please complete password field').isLength({ min: 1 }),
	sanitizeBody('userName').trim().escape(),
	sanitizeBody('pwd').trim().escape()
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.render('login', {
			errorMsg: errors.array(),
		});
	} else {
		userController.user_find_by_username_and_password(req, res);
	}
});

module.exports = router;