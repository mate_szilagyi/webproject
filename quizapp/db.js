var mongoose = require('mongoose');

//connect to database
var mongoURL = 'mongodb://localhost/quizappdb';
mongoose.connect(mongoURL, { poolSize: 10 });
mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.on('open', console.log.bind(console, 'MongoDB connection sucessfull'));
db.on('disconnected',function(){
	console.log('MongoDB disconnected!');
	mongoose.connect(mongoURL, { poolSize: 10 });
});
module.exports = db;