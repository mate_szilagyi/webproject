var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var mongoose = require('mongoose');

var registerRouter = require('./routes/register');
var loginRouter = require('./routes/login');
var homeRouter = require('./routes/home');
var controlRouter = require('./routes/control');
var quizRouter = require('./routes/quiz');

var app = express();

isAuthenticated = function (req, res, next) {
	session = req.session;
	if (session.uniqueID) {
		return next();
	}
	else {
		res.redirect(401, '/login');
	}
};

isAuthenticatedAsAdmin = function (req, res, next) {
	session = req.session;
	if (session.uniqueID) {
		if (session.uniqueID.role === 1) {
			return next()
		} else {
			res.redirect(403, '/home');
		}
	} else {
		res.redirect(401, '/login');
	}
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: "very_secret_string", resave: false, saveUninitialized: true, cookie: { maxAge: 3 * 3600000 } })); //maxage set for 3 hours

app.get('/alive', function (req, res) {
	if (mongoose.connection.readyState == 1) {
		res.status(200).send("very much");
	} else {
		res.status(500).send("database connection not set yet");
	}
});

app.use('/', loginRouter);
app.use('/login', loginRouter);
app.use('/register', registerRouter);
app.use(isAuthenticated)
app.use('/home', homeRouter);
app.use('/quiz', quizRouter);
app.use(isAuthenticatedAsAdmin);
app.use('/control', controlRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});



module.exports = app;